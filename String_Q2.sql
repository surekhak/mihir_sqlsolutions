SELECT A.[String],  
     Split.a.value('.', 'VARCHAR(100)') AS String  
 FROM  (SELECT [id],  
         CAST ('<M>' + REPLACE([string], ',', '</M><M>') + '</M>' AS XML) AS String  
     FROM  Table_1) AS A CROSS APPLY String.nodes ('/M') AS Split(a);