DECLARE @tbl TABLE(ID INT IDENTITY,phoneString VARCHAR(100));
INSERT INTO @tbl VALUES('7589586586'),('758ABC6586'),('758ABC65');

WITH tenDigits AS (SELECT Nr FROM(VALUES(1),(2),(3),(4),(5),(6),(7),(8),(9),(10)) AS x(Nr))
,Splitted AS
(
    SELECT ID 
          ,Nr
          ,CASE WHEN SUBSTRING(phoneString,Nr,1) IN('a','b','c','2') THEN '2'
                WHEN SUBSTRING(phoneString,Nr,1) IN('d','e','f','3') THEN '3'
                WHEN SUBSTRING(phoneString,Nr,1) IN('g','h','i','4') THEN '4'
                WHEN SUBSTRING(phoneString,Nr,1) IN('j','k','l','5') THEN '5'
                WHEN SUBSTRING(phoneString,Nr,1) IN('m','n','o','6') THEN '6'
                WHEN SUBSTRING(phoneString,Nr,1) IN('p','q','r','s','7') THEN '7'
                WHEN SUBSTRING(phoneString,Nr,1) IN('t','u','v','8') THEN '8'
                WHEN SUBSTRING(phoneString,Nr,1) IN('w','x','y','z','9') THEN '9'
                WHEN SUBSTRING(phoneString,Nr,1) ='1' THEN '1'
                WHEN SUBSTRING(phoneString,Nr,1) ='0' THEN '0'
                ELSE 'X'
            END AS Digit
    FROM @tbl AS t
    CROSS JOIN tenDigits
)
,ReConcatenated AS
(
    SELECT s.ID
          ,'(' 
          + STUFF(STUFF(
           (SELECT x.Digit AS [*] 
            FROM Splitted AS x 
            WHERE s.ID=x.ID 
            ORDER BY Nr
            FOR XML PATH('')
           ),4,0,') '),9,0,'-') AS PhoneNumber
    FROM Splitted AS s
    GROUP BY ID
)
SELECT *
      ,CASE WHEN CHARINDEX('X',PhoneNumber)>0 THEN 'Invalid Format' ELSE PhoneNumber END AS Validated
FROM ReConcatenated