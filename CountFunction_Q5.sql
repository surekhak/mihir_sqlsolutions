CREATE PROC COUNT_FUN
(
	@input INT
)
AS
with cte as
(
SELECT district_name, COUNT(district_name) AS cnt FROM table_district
group by district_name
)
select district_name from cte WHERE cnt=@input