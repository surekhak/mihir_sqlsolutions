SELECT t.district_id, t.population
FROM (
    SELECT *, Row_Number() OVER(ORDER BY district_id) AS RowNumber 
            --Row_Number() starts with 1
    FROM table_district
) t
WHERE t.RowNumber % 2 = 0 --Even

SELECT t.district_id, t.population
FROM (
    SELECT *, Row_Number() OVER(ORDER BY district_id) AS RowNumber 
            --Row_Number() starts with 1
    FROM table_district
) t
WHERE t.RowNumber % 2 = 1 --Odd