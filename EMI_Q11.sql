
CREATE PROC [dbo].[EMI]   
    @LoanAmount decimal(18,2),   
    @InterestRate decimal(18,2),   
    @LoanPeriod Int
    AS  
    BEGIN  
    SET NOCOUNT ON  
      
    DECLARE   
      
    @Payment decimal(12,2),   
    @Period FLOAT,   
    @Payment2 decimal(12,2),  
    @TotalPayment decimal(12,2),  
    @FinanceCharges FLOAT,  
    @CompoundingPeriod FLOAT,  
    @CompoundingInterest FLOAT,  
    @CurrentBalance decimal(12,2),  
    @Principal FLOAT,  
    @Interest FLOAT
      
      
      
    SET @InterestRate = @InterestRate/100   
      
    SET @CompoundingPeriod = 12   
      
      
    /*** END USER VARIABLES ***/   
      
    SET @CompoundingInterest = @InterestRate/@CompoundingPeriod   
      
    SET @Payment = ROUND((((@InterestRate/12) * @LoanAmount)/(1- ( POWER( (1 + (@InterestRate/12)),(-1 * @LoanPeriod) )))),2)   
      
    SET @TotalPayment = @Payment * @LoanPeriod   
      
    SET @FinanceCharges = @TotalPayment - @LoanAmount   
      
    IF EXISTS(SELECT object_id FROM tempdb.sys.objects WHERE name LIKE '#EMI%')   
      
    BEGIN   
      
    DROP TABLE #EMI   
      
    END   
      
    /*** IT'S A TEMPORERY TABLE ***/   
      
    CREATE TABLE #EMI(   
      
     PERIOD INT   
      
    ,PAYMENT decimal(12,2)   
      
    ,CURRENT_BALANCE decimal(12,2)   
      
    ,INTEREST decimal(12,2)   
      
    ,PRINCIPAL decimal(12,2)
      
    )   
      
    SET @Period = 1   
      
      
    BEGIN   
      
    WHILE (@Period < = @LoanPeriod)   
      
    BEGIN   
      
    SET @CurrentBalance = ROUND (@LoanAmount * POWER( (1+ @CompoundingInterest) , @Period ) - ( (ROUND(@Payment,2)/@CompoundingInterest) * (POWER((1 + @CompoundingInterest),@Period ) - 1)),0)   
      
    SET @Principal =   
    CASE   
    WHEN @Period = 1   
    THEN   
    ROUND((ROUND(@LoanAmount,0) - ROUND(@CurrentBalance,0)),0)   
    ELSE   
    ROUND ((SELECT ABS(ROUND(CURRENT_BALANCE,0) - ROUND(@CurrentBalance,0))   
    FROM #EMI   
    WHERE PERIOD = @Period -1),2)   
    END   
      
    SET @Interest = ROUND(ABS(ROUND(@Payment,2) - ROUND(@Principal,2)),2)   
      
 
      
    INSERT   
    #EMI  
      
    SELECT   
      
    @Period,      
    @Payment,   
    @CurrentBalance,   
    @Interest,   
    @Principal   
      
    SET @Period = @Period + 1 
      

      
    END   
      
    END   
      
    SELECT * FROM #EMI  
    END  
    GO
   exec EMI 3000000,10,48