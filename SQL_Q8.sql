DELIMITER $$ 
DROP function IF EXISTS `Population` $$
CREATE function Population( dist_name varchar(20)) returns varchar(500)

BEGIN
  
  declare i int;
  declare cnt int;
  
  declare output varchar(500);
  
  set i=0;
  set output="";

  set cnt=(select count(district_name) from tbl_district where district_name=dist_name);
   while i<cnt do
     set output=CONCAT(output,(select population from tbl_district where district_name=dist_name limit i,1),',');
   
    set i=i+1;
    end while;
       


   return output;
end;
$$ 
DELIMITER ;




/*
Output command-
select Population("Satara");
*/