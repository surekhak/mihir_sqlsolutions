CREATE FUNCTION fn_phonenumber(@input VARCHAR(20))
RETURNS VARCHAR(50)
  BEGIN
    DECLARE @compare VARCHAR(30) = ''
    DECLARE @cnt INT = 1
    DECLARE @varout VARCHAR(30) = ''
    DECLARE @val VARCHAR(30) = ''
    DECLARE @Phoutput VARCHAR(50) = ''
    DECLARE @var INT

    SET @var = LEN(@input)

    IF @var <> 10
        OR @input LIKE '%[^a-z0-9-'']%'
      SET @Phoutput = 'Invalid Format'
    ELSE
      BEGIN
        WHILE @cnt <= 10
          BEGIN
            SET @compare = SUBSTRING(@input, @cnt, 1)
            SET @val=CASE
                       WHEN @compare IN ('a', 'b', 'c', '2') THEN 2
                       WHEN @compare IN ('d', 'e', 'f', '3') THEN 3
                       WHEN @compare IN ('g', 'h', 'i', '4') THEN 4
                       WHEN @compare IN ('j', 'k', 'l', '5') THEN 5
                       WHEN @compare IN ('m', 'n', 'o', '6') THEN 6
                       WHEN @compare IN ('p', 'q', 'r', 's', '7') THEN 7
                       WHEN @compare IN ('t', 'u', 'v', '8') THEN 8
                       WHEN @compare IN ('w', 'x', 'y', 'z', '9') THEN 9
                       WHEN @compare IN ('1') THEN 1
                       WHEN @compare IN ('0') THEN 0
                       ELSE 0
                     END
            SET @varout = @varout + @val
            SET @cnt = @cnt + 1
          END
      END

    IF @Phoutput <> 'Invalid Format'
      BEGIN
        SET @Phoutput = ('('+ SUBSTRING(@varout, 1, 3)+ ')'+' '+ SUBSTRING(@varout, 4, 3)+ '-'+ SUBSTRING(@varout, 7, 4))
      END

    RETURN @Phoutput
  END 
  
  