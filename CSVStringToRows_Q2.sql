CREATE PROCEDURE [dbo].[CSVtoROWS]
(
  @S VARCHAR(100)
) -- CSVtoROWS 'zCon,Solutions,Raj,Business'
AS 
BEGIN

 SELECT Split.a.value('.', 'VARCHAR(100)') AS String  
 FROM  (SELECT CAST ('<M>' + REPLACE(@S, ',', '</M><M>') + '</M>' AS XML) AS String ) AS A 
 CROSS APPLY String.nodes ('/M') AS Split(a);

END

