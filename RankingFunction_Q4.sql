CREATE PROCEDURE [dbo].[Ranking] 
( 
	@district_name VARCHAR(100),
    @abc INT
)
AS
BEGIN
with cte as
(
SELECT district_id, district_name, population,
RANK() OVER (ORDER BY population desc) AS R1
FROM [master].[dbo].[table_district]
where district_name = @district_name
)select  district_name, population from cte where R1 = @abc order by population desc
END
