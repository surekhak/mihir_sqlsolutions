ALTER PROCEDURE [dbo].[sp_Encrypt]
(
@pClearString  nvarchar(100) OUTPUT
)
AS
BEGIN
DECLARE @vEncryptedString NVARCHAR(100)
DECLARE @vIdx INT
DECLARE @vBaseIncrement INT
SET @vIdx = 1
SET @vBaseIncrement = 128
SET @vEncryptedString = ''
WHILE @vIdx <= LEN(@pClearString)
BEGIN
SET @vEncryptedString = @vEncryptedString + NCHAR(ASCII( SUBSTRING(@pClearString, @vIdx, 1)) ^129)
SET @vIdx = @vIdx + 1
END
PRINT @vEncryptedString
END

Exec sp_Encrypt 'I am Mihir'